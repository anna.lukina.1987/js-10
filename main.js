const iconsPassword = document.querySelectorAll('.icon-password');
const buttonConfirm = document.querySelector('.button-confirm');
const inputWrappers = document.querySelectorAll('.input-wrapper');
const lastInputWrapper = inputWrappers[ inputWrappers.length - 1 ];
const inpPass = document.querySelector('.inpPass');
const inpPassConf = document.querySelector('.inpPassConf');

iconsPassword.forEach(function (icon) {
    icon.addEventListener('click', function () {
        const inpPass = icon.parentNode.querySelector('input');
        icon.parentNode.classList.toggle('show');
        const isShow = icon.parentNode.classList.contains('show');

        if ( isShow ) {
            inpPass.setAttribute('type', 'text');
        } else {
            inpPass.setAttribute('type', 'password');

        }
    })
})

buttonConfirm.addEventListener('click', function (e) {
    e.preventDefault();

    const inpPassVal = inpPass.value;
    const inpPassConfVal = inpPassConf.value;
    const isEqueals = inpPassVal === inpPassConfVal;
    const errorBlock = document.querySelector('.error-text');

    if(errorBlock) errorBlock.remove();

    if (isEqueals) {
        alert("You are welcome");
    } else {
        const errorBlock = document.createElement('div');
        errorBlock.classList.add('error-text')
        errorBlock.innerText = `Нужно ввести одинаковые значения`;
        errorBlock.style.color = 'red';
        lastInputWrapper.parentNode.insertBefore(errorBlock, lastInputWrapper.nextSibling);
    }
})